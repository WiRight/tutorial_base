import ProjectsService from '@/services/ProjectsService'
import CacheService from '@/services/CacheService'
import {gitlabAxios} from '@/common/gitlab_http'

/**
 * Сервис для работы с бандлами
 */
export default class BundleService {
  /**
   * Получение информации о бандле
   *
   * @param projectId Идентификатор проекта
   * @returns {Promise<any[]>}
   */
  static getBundle (projectId) {
    return Promise.all([
      ProjectsService.getTags(projectId),
      ProjectsService.getInfo(projectId),
    ]).then(data => {
      let result = {}

      data.forEach(it => {
        result = Object.assign(result, it)
      })

      return result
    })
  }

  /**
   * Получение информации из Readme.md
   *
   * @param projectId Идентификатор проекта
   * @returns {Promise<any>}
   */
  static getReadme (projectId) {
    return new Promise((resolve, reject) => {
      const cacheKey = `${projectId}_readme`

      if (!CacheService.getByKey(cacheKey)) {
        gitlabAxios.get(`/projects/${projectId}/repository/files/README.md?ref=master`)
          .then(response => {
            const result = {
              bundle: response.data,
            }

            CacheService.setValue(cacheKey, JSON.stringify(result))

            resolve(result)
          })
          .catch(reject)
      } else {
        resolve(
          JSON.parse(CacheService.getByKey(cacheKey))
        )
      }
    })
  }
}
