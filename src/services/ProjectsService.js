import {gitlabAxios} from '../common/gitlab_http'

import CacheService from './CacheService'

/**
 * Общий сервис для работы с проектами
 */
export default class ProjectsService {
  /**
   * Получение тегов репозитория
   *
   * @param projectId Идентификатор проекта
   * @returns {Promise<any>}
   */
  static getTags (projectId) {
    return new Promise((resolve, reject) => {
      const cacheKey = `${projectId}_tags`

      if (!CacheService.getByKey(cacheKey)) {
        gitlabAxios.get(`/projects/${projectId}/repository/tags`)
          .then(response => {
            const data = response.data
            const result = {
              tags: {
                latest: data[0], // TODO: не уверен что всегда тут будет именно новый
                list: data,
              },
            }

            CacheService.setValue(cacheKey, JSON.stringify(result))

            resolve(result)
          })
          .catch(reject)
      } else {
        resolve(
          JSON.parse(CacheService.getByKey(cacheKey))
        )
      }
    })
  }

  /**
   * Получение информации о проекте
   *
   * @param projectId Идентификатор проекта
   * @returns {Promise<any>}
   */
  static getInfo (projectId) {
    return new Promise((resolve, reject) => {
      const cacheKey = `${projectId}_info`

      if (!CacheService.getByKey(cacheKey)) {
        gitlabAxios.get(`/projects/${projectId}`)
          .then(response => {
            const result = {
              bundle: response.data,
            }

            CacheService.setValue(cacheKey, JSON.stringify(result))

            resolve(result)
          })
          .catch(reject)
      } else {
        resolve(
          JSON.parse(CacheService.getByKey(cacheKey))
        )
      }
    })
  }
}
