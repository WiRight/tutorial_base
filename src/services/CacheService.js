/**
 * Сервис для работы с кешированием
 *
 * Предоставляет доступ к созданию данных в кеше и прочитыванию
 */
export default class CacheService {
  /**
   * Получение значения по ключу
   *
   * @param {string} key Имя ключа
   * @returns {string}
   */
  static getByKey (key) {
    return localStorage.getItem(key)
  }

  /**
   * Установка значения в кеш
   *
   * @param {string} key Ключ
   * @param {string} value Значение
   */
  static setValue (key, value) {
    localStorage.setItem(key, value)
  }

  /**
   * Очистка кеша
   */
  static clear () {
    localStorage.clear()
  }
}
