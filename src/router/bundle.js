// MainModule
import Bundle from '../modules/bundles/Bundle'

// SubModules
import BundleList from '../modules/bundles/modules/list/BundleList'
import BundleView from '../modules/bundles/modules/view/BundleView'

export default [
  {
    path: '/bundles',
    component: Bundle,
    children: [
      { path: '', component: BundleList, },
      { path: ':id', component: BundleView, },
    ],
  },
]
