import Dashboard from '../modules/dashboard/Dashboard'

export default [
  {
    path: '/',
    component: Dashboard,
  },
]
