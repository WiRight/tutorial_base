/**
 * Миксин для добавления функционала по работе с ошибками модулям
 */
export default {
  watch: {
    // Следим когда появится ошибка
    error: function (val) {
      this.$store.commit('error/setMessage', val)
    },
  },
  data () {
    return {
      // Текст ошибки
      error: '',
    }
  },
}
