// Libs
import Vue from 'vue'
import Vuex from 'vuex'

// Vuex modules
import toolbar from './modules/toolbar'
import error from './modules/error'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    // Работа с Toolbar
    toolbar,
    // Работа с ошибками
    error,
  },
})
