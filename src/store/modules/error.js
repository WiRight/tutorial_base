const state = {
  title: '',
  message: '',
}

const getters = {
  title: state => state.title,
  message: state => state.message,
}

const mutations = {
  // Установка заголовка ошибки
  setTitle: function (state, title) {
    state.title = title
  },

  // Установка сообщения ошибки
  setMessage: function (state, message) {
    state.message = message
  },
}

const actions = {}

export default {
  strict: process.env.NODE_ENV !== 'production',
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
