const state = {
  title: '',
  subtitle: '',
}

const getters = {
  title: state => state.title,
}

const mutations = {
  // Установка заголовка в Toolbar
  setTitle: function (state, title) {
    state.title = title
  },

  // Установка подзаголовка от подмодуля
  setSubModuleTitle: function (state, title) {
    state.title = state.title.split('|')[0] + ' | ' + title
  },

  // Установка подзаголовка
  setSubTitle: function (state, subTitle) {
    state.subtitle = subTitle
  },
}

const actions = {}

export default {
  strict: process.env.NODE_ENV !== 'production',
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
