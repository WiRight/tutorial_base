// Lobs
import Vue from 'vue'

// Store
import {store} from './store/store'

// Local components
import App from './App.vue'

// Router
import router from './router'

// Quasar
import './quasar'

Vue.config.productionTip = process.env.NODE_ENV !== 'production'

new Vue({
  render: h => h(App),
  store,
  router,
}).$mount('#app')
