// Libs
import Vue from 'vue'
import Router from 'vue-router'

// Module routes
import dashboardRoutes from './router/dashboard'
import bundleRoutes from './router/bundle'

Vue.use(Router)

export default new Router({
  routes: [
    ...dashboardRoutes,
    ...bundleRoutes,
  ],
})
