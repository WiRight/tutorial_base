import Vue from 'vue'

import './styles/quasar.styl'
import 'quasar/dist/quasar.ie.polyfills'
import '@quasar/extras/material-icons/material-icons.css'
import {
  Quasar,
  QAjaxBar,
  QLayout,
  QHeader,
  QDrawer,
  QPageContainer,
  QPage,
  QCard,
  QCardSection,
  QCardActions,
  QCircularProgress,
  QImg,
  QSpace,
  QSeparator,
  QToolbar,
  QToolbarTitle,
  QBtn,
  QIcon,
  QList,
  QItem,
  QItemSection,
  QItemLabel,
  Ripple,
  QForm,
  QInput
} from 'quasar'

Vue.use(Quasar, {
  config: {},
  components: {
    QAjaxBar,
    QLayout,
    QHeader,
    QDrawer,
    QPageContainer,
    QPage,
    QCard,
    QCardSection,
    QCardActions,
    QCircularProgress,
    QImg,
    QSpace,
    QSeparator,
    QToolbar,
    QToolbarTitle,
    QBtn,
    QIcon,
    QList,
    QItem,
    QItemSection,
    QItemLabel,
    QForm,
    QInput,
  },
  directives: {
    Ripple,
  },
  plugins: {},
  animations: 'all',
})
